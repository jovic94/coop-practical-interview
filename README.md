# Coop Bank Practical Interview

This project contains practical assesment work to the interview of Cooperative Bank for Frontend Development.

## Getting Started

To begin accessing this codebase, ensure:
* You have latest node version installed in your machine
* You have angular cli installed in your machine
* You have ionic cli installed in your machine

## Start the project
The project is started with the regular ionic commands.

1. Run `npm install` to install all dependencies.
2. Run `ionic serve` to start the development environment.
3. To build the project run `ionic build android` or `ionic build ios`. In order for you to build an iOS app, you need to run on MacOS.


## Creator

**Jerry Victor Odhiambo**
